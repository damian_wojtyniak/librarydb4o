﻿using System.Collections.Generic;

namespace Library.Models
{
    public class Reader
    {
        public int Number { get; set; }
        public ICollection<Book> Loans { get; set; }

        public Reader(int number)
        {
            Number = number;
        }

        public override string ToString()
        {
            return $"{Number}";
        }
    }
}
