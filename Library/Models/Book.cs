﻿using System.Collections.Generic;

namespace Library.Models
{
    public class Book
    {
        public string Title { get; set; }
        public string Publisher { get; set; }
        public int Year { get; set; }

        public virtual Author Author { get; set; }
        public virtual Reader ActualReader { get; set; }
        
        public Book(string title, string publisher, int year)
        {
            Title = title;
            Publisher = publisher;
            Year = year;
        }

        public override string ToString()
        {
            return $"{Title}, {Publisher}, {Year}";
        }
    }
}