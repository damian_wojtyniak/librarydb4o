﻿using System.Collections.Generic;

namespace Library.Models
{
    public class Author
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public ICollection<Book> Books { get; set; }

        public Author(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public override string ToString()
        {
            return $"{FirstName}, {LastName}";
        }
    }
}

