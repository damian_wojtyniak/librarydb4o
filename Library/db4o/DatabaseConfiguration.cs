﻿using Db4objects.Db4o;
using Db4objects.Db4o.Config;

namespace Library.db4o
{
    class DatabaseConfiguration
    {
        public static IObjectContainer GetDatabaseConnection()
        {
            IObjectContainer database = Db4oFactory.OpenFile(GetDatabaseConfiguration(), "../../Database/db4oLibraryDb.yap");
            
            return database;
        }

        private static IConfiguration GetDatabaseConfiguration()
        {
            IConfiguration configuration = Db4oFactory.Configure();
            configuration.MessageLevel(0);

            return configuration;
        }
    }
}
