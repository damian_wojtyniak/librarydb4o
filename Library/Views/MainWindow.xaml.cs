﻿using Db4objects.Db4o;
using Library.db4o;
using Library.Models;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace RemoteCourses.Views
{
    public partial class MainWindow : Window
    {
        IObjectContainer databaseContainer;

        public MainWindow()
        {
            InitializeComponent();

            databaseContainer = DatabaseConfiguration.GetDatabaseConnection();

            //SeedDatabase();
        }

        private void SeedDatabase()
        {
            var autor1 = new Author("Damian", "Wojtyniak");
            var autor2 = new Author("Marek", "Koziol");
            var autor3 = new Author("Samanta", "Jazz");

            var book1 = new Book("Dobra książka", "Dobry publisher", 2020) { Author = autor1 };
            var book2 = new Book("Władca książki", "Janowo", 1974) { Author = autor2 };
            var book3 = new Book("Cieka Rzeka", "Myslow", 1999) { Author = autor3 };

            var reader1 = new Reader(1111);
            var reader2 = new Reader(1234);
            var reader3 = new Reader(1333);

            autor1.Books = new List<Book>() { book1 };
            autor2.Books = new List<Book>() { book2 };
            autor3.Books = new List<Book>() { book3 };

            databaseContainer.Store(autor1);
            databaseContainer.Store(autor2);
            databaseContainer.Store(autor3);
            databaseContainer.Store(book1);
            databaseContainer.Store(book2);
            databaseContainer.Store(book3);
            databaseContainer.Store(reader1);
            databaseContainer.Store(reader2);
            databaseContainer.Store(reader3);
        }

        private void RefreshReadersMethod()
        {
            var readers = databaseContainer.Query<Reader>(typeof(Reader));

            readersListBox.ItemsSource = null;
            readersListBox.ItemsSource = readers;
        }

        private void RefreshAuthorsMethod()
        {
            var authors = databaseContainer.Query<Author>(typeof(Author));

            authorsListBox.ItemsSource = null;
            authorsListBox.ItemsSource = authors;
        }

        private void RefreshBooksMethod()
        {
            var books = databaseContainer.Query<Book>(typeof(Book));
            var readers = databaseContainer.Query<Reader>(typeof(Reader));

            booksListBox.ItemsSource = null;
            booksListBox.ItemsSource = books;

            borrowReadersListBox.ItemsSource = null;
            borrowReadersListBox.ItemsSource = readers;
        }

        private void RefreshReaders(object sender, RoutedEventArgs e)
        {
            RefreshReadersMethod();
        }

        private void AddReader(object sender, RoutedEventArgs e)
        {
            bool validation = int.TryParse(addReaderNumberTextBox.Text, out int value);
            if (!validation) return;

            Reader newReader = new Reader(value);

            databaseContainer.Store(newReader);
            RefreshReadersMethod();
        }

        private void RefreshAuthors(object sender, RoutedEventArgs e)
        {
            RefreshAuthorsMethod();
        }

        private void RefresBooks(object sender, RoutedEventArgs e)
        {
            RefreshBooksMethod();
        }

        private void BorrowBook(object sender, RoutedEventArgs e)
        {
            var book = booksListBox.SelectedItem as Book;
            var reader = borrowReadersListBox.SelectedItem as Reader;

            if (reader == null || book == null)
                return;

            if (book.ActualReader != null)
            {
                MessageBox.Show("Ta książka została już wypożyczona");
                return;
            }

            book.ActualReader = reader;

            databaseContainer.Store(book);

            MessageBox.Show("Wypożyczono ksiązkę");
        }

        private void AcceptReturn(object sender, RoutedEventArgs e)
        {
            var book = booksListBox.SelectedItem as Book;
            var reader = borrowReadersListBox.SelectedItem as Reader;

            if (reader == null || book == null)
                return;

            if (book.ActualReader != reader)
            {
                MessageBox.Show("Wybrany użytkownik nie jest w posiadaniu tej książki");
                return;
            }

            book.ActualReader = null;

            databaseContainer.Store(book);

            MessageBox.Show("Zwrócono książkę");
        }

        private void authorsBooksDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var author = authorsListBox.SelectedItem as Author;
            var books = author.Books;

            authorBooksListBox.ItemsSource = null;
            authorBooksListBox.ItemsSource = books;
        }

        private void RemoveReader(object sender, RoutedEventArgs e)
        {
            var reader = readersListBox.SelectedItem as Reader;

            databaseContainer.Delete(reader);
            RefreshReadersMethod();
        }
    }
}
